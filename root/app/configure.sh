#! /bin/sh

cat /app/config.json | sed \
  -e "s/{{BRIDGE_USERNAME}}/${BRIDGE_USERNAME}/g" \
  -e "s/{{BRIDGE_PIN}}/${BRIDGE_PIN}/g" \
  -e "s/{{BRIDGE_SETUPID}}/${BRIDGE_SETUPID}/g" \
  -e "s/{{PURPLEAIR_ID}}/${PURPLEAIR_ID}/g" \
  -e "s/{{UPDATE_FREQ}}/${UPDATE_FREQ}/g" \
  -e "s/{{ADJUST}}/${ADJUST}/g" \
  -e "s/{{STATSKEY}}/${STATSKEY}/g" \
  -e "s/{{INCLUDEPM10}}/${INCLUDEPM10}/g" \
  > /app/homebridge/config.json
